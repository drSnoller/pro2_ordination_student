package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {
	Laegemiddel l1, l2, l3;
	PN pn1, pn2, pn3;

	@Before
	public void setUp() throws Exception {
		pn1 = new PN(LocalDate.of(2019, 04, 01), LocalDate.of(2019, 06, 04), l1, 5);
		pn2 = new PN(LocalDate.of(2015, 12, 12), LocalDate.of(2016, 01, 01), l2, 5);
		pn3 = new PN(LocalDate.of(2018, 04, 06), LocalDate.of(2018, 04, 10), l3, 5);
		
		l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		l2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		l3 = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
	}

	@Test
	public void testSamletDosis() {
		//TC1
		boolean givetDato = pn1.givDosis(LocalDate.of(2019, 04, 06));
		boolean givetDato2 = pn1.givDosis(LocalDate.of(2019, 04, 07));
		boolean givetDato3 = pn1.givDosis(LocalDate.of(2019, 04, 8));
		boolean givetDato4 = pn1.givDosis(LocalDate.of(2019, 04, 9));
		assertTrue(givetDato);
		assertTrue(givetDato2);
		assertTrue(givetDato3);
		assertTrue(givetDato4);
		assertNotNull(pn1);
		assertEquals(20.00, pn1.samletDosis(), 0.1);
		
		//TC2
		boolean givetDato5 = pn2.givDosis(LocalDate.of(2015, 12, 16));
		boolean givetDato6 = pn2.givDosis(LocalDate.of(2015, 12, 17));
		boolean givetDato7 = pn2.givDosis(LocalDate.of(2015, 12, 18));
		boolean givetDato8 = pn2.givDosis(LocalDate.of(2015, 12, 19));
		boolean givetDato9 = pn2.givDosis(LocalDate.of(2015, 12, 20));
		boolean givetDato10 = pn2.givDosis(LocalDate.of(2015, 12, 22));
		boolean givetDato11 = pn2.givDosis(LocalDate.of(2015, 12, 23));
		assertTrue(givetDato5);
		assertTrue(givetDato6);
		assertTrue(givetDato7);
		assertTrue(givetDato8);
		assertTrue(givetDato9);
		assertTrue(givetDato10);
		assertTrue(givetDato11);
		assertEquals(35.00, pn2.samletDosis(), 0.1);

		//TC3
		boolean givetDato12 = pn3.givDosis(LocalDate.of(2018, 04, 07));
		boolean givetDato13 = pn3.givDosis(LocalDate.of(2018, 04, 8));
		assertTrue(givetDato12);
		assertTrue(givetDato13);
		assertEquals(10.00, pn3.samletDosis(), 0.1);
	
	}

	@Test
	public void testDoegnDosis() {
		//TC1
		pn1.givDosis(LocalDate.of(2019, 05, 02));
		pn1.givDosis(LocalDate.of(2019,05, 03));
		pn1.givDosis(LocalDate.of(2019,05, 03));
		pn1.givDosis(LocalDate.of(2019, 05, 04));
		assertNotNull(pn1);
		assertEquals(6.66, pn1.doegnDosis(), 0.1);
		
		//TC2
		pn2.givDosis(LocalDate.of(2015, 12, 12));
		pn2.givDosis(LocalDate.of(2015, 12, 14));
		pn2.givDosis(LocalDate.of(2015, 12, 14));
		pn2.givDosis(LocalDate.of(2015, 12, 20));
		pn2.givDosis(LocalDate.of(2015, 12, 21));
		pn2.givDosis(LocalDate.of(2015, 12, 30));
		assertNotNull(pn2);
		assertEquals(1.57, pn2.doegnDosis(), 0.1);
		
		//TC3
		pn3.givDosis(LocalDate.of(2018, 04, 07));
		pn3.givDosis(LocalDate.of(2018, 04, 9));
		pn3.givDosis(LocalDate.of(2018, 04, 9));
		assertNotNull(pn3);
		assertEquals(5.00, pn3.doegnDosis(), 0.1);	
		
	}

	@Test
	public void testPN() {
		//TC1
		assertNotNull(pn1);
		assertEquals(LocalDate.of(2019, 04, 01), pn1.getStartDen());
		
		//TC2
		assertNotNull(pn2);
		assertEquals(LocalDate.of(2015, 12, 12), pn2.getStartDen());
		
		//TC3
		assertNotNull(pn3);
		assertEquals(LocalDate.of(2018, 04, 06), pn3.getStartDen());
	}

	@Test
	public void testGivDosis() {
		//TC1
		boolean givetDato = pn1.givDosis(LocalDate.of(2019, 01, 06));
		boolean givetDato2 = pn1.givDosis(LocalDate.of(2019, 01, 01));
		assertFalse(givetDato);
		assertFalse(givetDato2);
		assertEquals(0, pn1.getAntalGangeGivet());

		//TC2
		boolean givetDato3 = pn2.givDosis(LocalDate.of(2015, 12, 16));
		boolean givetDato4 = pn2.givDosis(LocalDate.of(2015, 01, 07));
		assertTrue(givetDato3);
		assertFalse(givetDato4);
		assertEquals(1, pn2.getAntalGangeGivet());

		//TC3
		boolean givetDato5 = pn3.givDosis(LocalDate.of(2018, 04, 06));
		boolean givetDato6 = pn3.givDosis(LocalDate.of(2018, 04, 05));
		assertFalse(givetDato5);
		assertFalse(givetDato6);
		assertEquals(0, pn3.getAntalGangeGivet());
	}

}
