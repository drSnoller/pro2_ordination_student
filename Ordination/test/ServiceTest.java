package test;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class ServiceTest {
    Service service;
    Patient p1, p2, p3, p4, p5;
    Laegemiddel l1, l2, l3, l4;

    @Before
    public void setUp() throws Exception {
        service = Service.getTestService();

        p1 = service.opretPatient("121256-0512", "Jane Jensen", 63.4);
        p2 = service.opretPatient("070985-1153", "Finn Madsen", 83.2);
        p3 = service.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
        p4 = service.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
        p5 = service.opretPatient("090149-2529", "Ib Hansen", 87.7);

        l1 = service.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        l2 = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        l3 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        l4 = service.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretPNOrdination() {
        // TC1
        service.opretPNOrdination(LocalDate.of(2019, 05, 01), LocalDate.of(2019, 04, 04), p1, l1, 5);
    }

    @Test
    public void testOpretPNOrdination2() {
        // TC2
        PN pn2 = service.opretPNOrdination(LocalDate.of(2015, 12, 12), LocalDate.of(2016, 01, 01), p2, l2, 5);
        assertNotNull(pn2);
        assertEquals(LocalDate.of(2015, 12, 12), pn2.getStartDen());
        assertEquals(LocalDate.of(2016, 01, 01), pn2.getSlutDen());
        assertEquals(l2, pn2.getLaegemiddel());
        assertEquals(5, pn2.getAntalEnheder(), 0.1);

        // TC3
        PN pn3 = service.opretPNOrdination(LocalDate.of(2018, 04, 06), LocalDate.of(2018, 04, 10), p3, l3, 5);
        assertNotNull(pn3);
        assertEquals(LocalDate.of(2018, 04, 06), pn3.getStartDen());
        assertEquals(LocalDate.of(2018, 04, 10), pn3.getSlutDen());
        assertEquals(l3, pn3.getLaegemiddel());
        assertEquals(5, pn3.getAntalEnheder(), 0.1);
    }

    @Test
    public void testOpretDagligFastOrdination() {
        // TestCase 1
        DagligFast dF = service.opretDagligFastOrdination(LocalDate.of(2018, 03, 6), LocalDate.of(2018, 03, 9), p1, l1,
                1.0, 2.0, 3.0, 4.0);
        assertNotNull(dF);
        assertEquals(LocalDate.of(2018, 03, 6), dF.getStartDen());
        assertEquals(LocalDate.of(2018, 03, 9), dF.getSlutDen());
        assertEquals(l1, dF.getLaegemiddel());

        assertEquals(1.0, dF.getDoser()[0].getAntal(), 0.0);
        assertEquals(2.0, dF.getDoser()[1].getAntal(), 0.0);
        assertEquals(3.0, dF.getDoser()[2].getAntal(), 0.0);
        assertEquals(4.0, dF.getDoser()[3].getAntal(), 0.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligFastOrdination2() {
        // TestCase 2
        @SuppressWarnings("unused")
        DagligFast dF2 = service.opretDagligFastOrdination(LocalDate.of(2018, 03, 6), LocalDate.of(2018, 03, 5), p1, l1,
                1.0, 2.0, 3.0, 4.0);

    }

    @Test(expected = NullPointerException.class)
    public void testOpretDagligFastOrdination3() {
        // TestCase 3
        DagligFast dF3 = service.opretDagligFastOrdination(LocalDate.of(2018, 03, 1), LocalDate.of(2018, 03, 3), p1,
                null, 1.0, 2.0, 3.0, 4.0);
    }

    @Test
    public void testOpretDagligSkaevOrdinationTC1() {
        // TC1
        LocalTime[] t1 = { LocalTime.of(10, 00), LocalTime.of(18, 00) };
        double[] a1 = { 5, 2 };
        Ordination ds1 = service.opretDagligSkaevOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 03), p1,
                l1, t1, a1);
        assertNotNull(ds1);
        assertEquals(l1, ds1.getLaegemiddel());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdinationTC2() {
        // TC2
        LocalTime[] t1 = { LocalTime.of(10, 00), LocalTime.of(18, 00) };
        double[] a1 = { 5, 2 };
        @SuppressWarnings("unused")
        Ordination ds1 = service.opretDagligSkaevOrdination(LocalDate.of(2018, 01, 03), LocalDate.of(2018, 01, 01), p1,
                l1, t1, a1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdinationTC3() {
        // TC3
        LocalTime[] t1 = { LocalTime.of(10, 00), LocalTime.of(18, 00) };
        double[] a1 = { 5 };
        @SuppressWarnings("unused")
        Ordination ds1 = service.opretDagligSkaevOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 03), p1,
                l1, t1, a1);
    }

    @Test(expected = NullPointerException.class)
    public void testOpretDagligSkaevOrdinationTC4() {
        // TC4
        LocalTime[] t1 = { LocalTime.of(10, 00), LocalTime.of(18, 00) };
        double[] a1 = { 5, 2 };
        @SuppressWarnings("unused")
        Ordination ds1 = service.opretDagligSkaevOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 03),
                null, l1, t1, a1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdinationTC5() {
        // TC5
        LocalTime[] t1 = { LocalTime.of(10, 00), LocalTime.of(18, 00) };
        double[] a1 = { 5, 2 };
        @SuppressWarnings("unused")
        Ordination ds1 = service.opretDagligSkaevOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 03), p1,
                null, t1, a1);
    }

    @Test
    public void testOrdinationPNAnvendt() {
        // TC1
        PN pn1 = service.opretPNOrdination(LocalDate.of(2019, 05, 01), LocalDate.of(2019, 06, 04), p1, l1, 5);
        service.ordinationPNAnvendt(pn1, LocalDate.of(2019, 05, 16));
        // TC2
        PN pn2 = service.opretPNOrdination(LocalDate.of(2015, 12, 12), LocalDate.of(2016, 01, 01), p2, l2, 5);
        service.ordinationPNAnvendt(pn2, LocalDate.of(2015, 12, 17));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrdinationPNAnvendtMedException() {
        // TC3
        PN pn3 = service.opretPNOrdination(LocalDate.of(2018, 04, 06), LocalDate.of(2018, 04, 10), p3, l3, 5);
        service.ordinationPNAnvendt(pn3, LocalDate.of(2018, 04, 05));

    }

}
