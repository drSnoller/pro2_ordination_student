package test;

import static org.junit.Assert.*;
import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import ordination.DagligFast;
import ordination.Laegemiddel;

public class DagligFastTest {
    Laegemiddel l1;
    DagligFast dF;

    @Before
    public void setUp() throws Exception {
        l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        dF = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), l1, 1.0, 2.0, 3.0, 4.0);
    }

    @Test
    public void testSamletDosis() {
        // TestCase 1
        assertEquals(20.0, dF.samletDosis(), 0.0);
        // TestCase 2
        DagligFast dF = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01), l1, 1.0, 2.0, 3.0, 4.0);
        assertEquals(10.0, dF.samletDosis(), 0.0);
        // TestCase 3
        DagligFast dF2 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2019, 01, 01), l1, 1.0, 2.0, 3.0, 4.0);
        assertEquals(3660.0, dF2.samletDosis(), 0.0);
    }

    @Test
    public void testDoegnDosis() {
        // TestCase 1
        assertEquals(10.0, dF.doegnDosis(), 0.0);
        // TestCase 2
        DagligFast dF2 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), l1, 1.0, 1.0, 1.0, 1.0);
        assertEquals(4.0, dF2.doegnDosis(), 0.0);
        // TestCase 3
        DagligFast dF3 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), l1, 2.0, 2.0, 2.0, 2.0);
        assertEquals(8.0, dF3.doegnDosis(), 0.0);
    }

    @Test
    public void testDagligFast() {
        // TestCase 1
        assertNotNull(dF);
        assertEquals(LocalDate.of(2018, 01, 01), dF.getStartDen());
        assertEquals(LocalDate.of(2018, 01, 02), dF.getSlutDen());
        assertEquals(l1, dF.getLaegemiddel());
        assertEquals(1.0, dF.getDoser()[0].getAntal(), 0.00);
        assertEquals(2.0, dF.getDoser()[1].getAntal(), 0.00);
        assertEquals(3.0, dF.getDoser()[2].getAntal(), 0.00);
        assertEquals(4.0, dF.getDoser()[3].getAntal(), 0.00);

        // TestCase 2
        DagligFast dF2 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01), l1, 2.0, 2.0, 2.0, 2.0);
        assertNotNull(dF2);
        assertEquals(LocalDate.of(2018, 01, 01), dF2.getStartDen());
        assertEquals(LocalDate.of(2018, 01, 01), dF2.getSlutDen());
        assertEquals(l1, dF2.getLaegemiddel());
        assertEquals(2.0, dF2.getDoser()[0].getAntal(), 0.00);
        assertEquals(2.0, dF2.getDoser()[1].getAntal(), 0.00);
        assertEquals(2.0, dF2.getDoser()[2].getAntal(), 0.00);
        assertEquals(2.0, dF2.getDoser()[3].getAntal(), 0.00);

        // TestCase 3
        DagligFast dF3 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 8), l1, 1.0, 1.0, 1.0, 1.0);
        assertNotNull(dF3);
        assertEquals(LocalDate.of(2018, 01, 01), dF3.getStartDen());
        assertEquals(LocalDate.of(2018, 01, 8), dF3.getSlutDen());
        assertEquals(l1, dF3.getLaegemiddel());
        assertEquals(1.0, dF3.getDoser()[0].getAntal(), 0.00);
        assertEquals(1.0, dF3.getDoser()[1].getAntal(), 0.00);
        assertEquals(1.0, dF3.getDoser()[2].getAntal(), 0.00);
        assertEquals(1.0, dF3.getDoser()[3].getAntal(), 0.00);
    }

}
