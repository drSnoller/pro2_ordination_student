package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;

public class DagligSkaevTest {
    DagligSkaev ds;
    Laegemiddel l1;
    LocalDate ld1, ld2;

    @Before
    public void setUp() throws Exception {
        l1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        ld1 = LocalDate.of(2018, 01, 01);
        ld2 = LocalDate.of(2018, 01, 02);
        ds = new DagligSkaev(ld1, ld1, l1);
        ds.opretDosis(LocalTime.of(10, 00), 10);
        ds.opretDosis(LocalTime.of(12, 00), 20);

    }

    @Test
    public void testSamletDosis() {
        // TC1
        ds.removeDosis(ds.getDoser().get(1));
        assertEquals(10, ds.samletDosis(), 0.1);

        // TC2
        DagligSkaev d1 = new DagligSkaev(ld1, ld2, l1);
        d1.opretDosis(LocalTime.of(10, 00), 10);
        assertEquals(20, d1.samletDosis(), 0.1);

        // TC3
        DagligSkaev d2 = new DagligSkaev(ld1, LocalDate.of(2018, 02, 01), l1);
        d2.opretDosis(LocalTime.of(10, 00), 10);
        assertEquals(320, d2.samletDosis(), 0.1);

        // TC4
        DagligSkaev d3 = new DagligSkaev(ld1, LocalDate.of(2019, 01, 01), l1);
        d3.opretDosis(LocalTime.of(10, 00), 10);
        assertEquals(3660, d3.samletDosis(), 0.1);

        // TC5
        DagligSkaev d4 = new DagligSkaev(ld1, LocalDate.of(2018, 01, 07), l1);
        d4.opretDosis(LocalTime.of(10, 00), 10);
        d4.opretDosis(LocalTime.of(12, 00), 1.1);
        d4.opretDosis(LocalTime.of(14, 00), 2.5);
        assertEquals(95.2, d4.samletDosis(), 0.1);

        // TC6
        DagligSkaev d5 = new DagligSkaev(ld2, ld1, l1);
        d5.opretDosis(LocalTime.of(10, 00), 10);
        assertEquals(0, d5.samletDosis(), 0.1);

        // TC7
        DagligSkaev d6 = new DagligSkaev(ld1, ld1, l1);
        assertEquals(0, d6.samletDosis(), 0.1);

    }

    @Test
    public void testDoegnDosis() {
        // TC1
        ds.removeDosis(ds.getDoser().get(1));
        assertEquals(10, ds.doegnDosis(), 0.1);

        // TC2
        ds.opretDosis(LocalTime.of(10, 00), 1.1);
        ds.opretDosis(LocalTime.of(10, 00), 2.5);
        assertEquals(13.6, ds.doegnDosis(), 0.1);

        // TC3
        ds.removeDosis(ds.getDoser().get(0));
        ds.removeDosis(ds.getDoser().get(0));
        ds.removeDosis(ds.getDoser().get(0));
        ds.opretDosis(LocalTime.of(10, 00), 0);
        assertEquals(0, ds.doegnDosis(), 0.1);

        // TC4
        ds.removeDosis(ds.getDoser().get(0));
        assertEquals(0, ds.doegnDosis(), 0.1);

    }

    @Test
    public void testGetType() {
        assertEquals("DagligSkæv", ds.getType());
    }

    @Test
    public void testDagligSkaev() {
        // TC1
        DagligSkaev d1 = new DagligSkaev(ld1, ld2, l1);
        assertNotNull(d1);
        assertEquals(ld1, d1.getStartDen());

        // TC2
        DagligSkaev d2 = new DagligSkaev(ld1, LocalDate.of(2017, 12, 31), l1);
        assertNotNull(d2);
        assertEquals(ld1, d2.getStartDen());

        // TC3
        DagligSkaev d3 = new DagligSkaev(ld1, ld2, null);
        assertNotNull(d3);
        assertEquals(ld1, d3.getStartDen());
        
    }

    @Test
    public void testOpretDosis() {
        // TC1
        ds.opretDosis(LocalTime.of(10, 00), 10);
        Dosis d1 = ds.getDoser().get(2);
        assertEquals(LocalTime.of(10, 00), d1.getTid());
        assertEquals(10.0, d1.getAntal(), 0.1);

        ds.opretDosis(LocalTime.of(12, 00), 20);
        Dosis d2 = ds.getDoser().get(3);
        assertEquals(LocalTime.of(12, 00), d2.getTid());
        assertEquals(20.0, d2.getAntal(), 0.1);
    }

    @Test
    public void testGetDoser() {
        // TC2
        assertEquals(2, ds.getDoser().size());
    }

    @Test
    public void testRemoveDosis() {
        // TC3
        Dosis d1 = ds.getDoser().get(0);
        Dosis d2 = ds.getDoser().get(1);
        int size = ds.getDoser().size();

        assertEquals(2, size, 0.1);

        ds.removeDosis(d1);
        ds.removeDosis(d2);
        size = ds.getDoser().size();

        assertEquals(0, size, 0.1);
    }

}
