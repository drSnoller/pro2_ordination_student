package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

    private double antalEnheder;
    private ArrayList<LocalDate> dosisDatoer = new ArrayList<>();

    public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {
        super(startDen, slutDen, laegemiddel);
        this.antalEnheder = antalEnheder;
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
     * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     *
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen) {
        boolean dosisGivet = false;

        if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen())) {
            dosisGivet = true;
            dosisDatoer.add(givesDen);
        }

        return dosisGivet;
    }

    /**
     * Returnerer antal gange ordinationen er anvendt
     *
     * @return
     */
    public int getAntalGangeGivet() {
        return dosisDatoer.size();
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

    @Override
    public double doegnDosis() {
        double doegnDosis = 0;
        LocalDate min = LocalDate.MAX;
        LocalDate max = LocalDate.MIN;
        for (LocalDate d : dosisDatoer) {
            if (d.isBefore(min)) {
                min = d;
            }
            if (d.isAfter(max)) {
                max = d;
            }
        }

        doegnDosis = samletDosis() / (ChronoUnit.DAYS.between(min, max) + 1);
        return doegnDosis;
    }

    @Override
    public double samletDosis() {
        double samletDosis = 0;
        samletDosis = getAntalGangeGivet() * antalEnheder;
        return samletDosis;
    }

    @Override
    public String getType() {
        return "PN";
    }

}
