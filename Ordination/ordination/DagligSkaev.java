package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
    private ArrayList<Dosis> doser = new ArrayList<>();

    public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
        super(startDen, slutDen, laegemiddel);

    }

    public void opretDosis(LocalTime tid, double antal) {
        Dosis dosis = new Dosis(tid, antal);
        doser.add(dosis);
    }

    public ArrayList<Dosis> getDoser() {
        return new ArrayList<>(doser);
    }

    public void removeDosis(Dosis dosis) {
        doser.remove(dosis);
    }

    @Override
    public double samletDosis() {
        double antalDoser = 0;
        antalDoser += doegnDosis() * super.antalDage();
        return antalDoser;
    }

    @Override
    public double doegnDosis() {
        double doegnDosis = 0;
        if (!doser.isEmpty()) {
            for (Dosis d : doser) {
                doegnDosis += d.getAntal();
            }
        }
        return doegnDosis;
    }

    @Override
    public String getType() {
        return "DagligSkæv";
    }

}
