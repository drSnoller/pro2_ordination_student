package ordination;

import java.time.*;
import java.util.Arrays;

public class DagligFast extends Ordination {
    public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal,
            double middagAntal, double aftenAntal, double natAntal) {
        super(startDen, slutDen, laegemiddel);

        doser[0] = new Dosis(LocalTime.of(6, 00), morgenAntal);
        doser[1] = new Dosis(LocalTime.of(12, 00), middagAntal);
        doser[2] = new Dosis(LocalTime.of(18, 00), aftenAntal);
        doser[3] = new Dosis(LocalTime.of(00, 00), natAntal);
    }

    private Dosis[] doser = new Dosis[4];

    public Dosis[] getDoser() {
        return Arrays.copyOfRange(doser, 0, doser.length);
    }

    @Override
    public double samletDosis() {
        return doegnDosis() * super.antalDage();
    }

    @Override
    public double doegnDosis() {
        double antalDoser = doser[0].getAntal() + doser[1].getAntal() + doser[2].getAntal() + doser[3].getAntal();
        return antalDoser;
    }

    @Override
    public String getType() {
        return "Daglig Fast";
    }
}
